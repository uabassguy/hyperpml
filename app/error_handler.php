<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dave
 * Date: 10/24/13
 * Time: 5:51 PM
 * To change this template use File | Settings | File Templates.
 */

$fatal_log_file = BASE .'/log/fatal.log';
$non_fatal_log_file = BASE .'/log/error.log';

if (!file_exists(BASE.'/log/')) {
    mkdir(BASE.'/log/', 0744, true);
}

function fatal_handler() {
    //if (!DEV_MODE) return;
    global $fatal_log_file;
    $errfile = "unknown file";
    $errstr  = "shutdown";
    $errno   = E_CORE_ERROR;
    $errline = 0;

    $error = error_get_last();
    echo $error['type'];
    if( $error !== NULL) {
        $errno   = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr  = $error["message"];
        error_log( format_error( $errno, $errstr, $errfile, $errline ), 3, $fatal_log_file );
        if (VERBOSE_MODE) {
            echo "<pre>"; print_r($error); echo "</pre>";
        }
    }
}

function non_fatal_handler($errno, $errstr, $errfile, $errline) {
    if (!DEV_MODE) return;
    global $non_fatal_log_file;
    $error = array('errno'=>$errno,'errstr'=>$errstr,'errfile'=>$errfile,'errline'=>$errline );
    if (LOG_MODE) error_log( format_error( $errno, $errstr, $errfile, $errline ), 3, $non_fatal_log_file );
    if (VERBOSE_MODE) {
        echo "<pre>";
        print_r($error);
        echo "</pre>";
    }
}

function format_error( $errno, $errstr, $errfile, $errline ) {
    $trace = print_r( debug_backtrace( false ), true );

    $content = "[".date('Y/m/d H:i:s')."]\n  Error: $errstr\n";
    $content .= "  Errno: $errno\n";
    $content .= "  File: $errfile\n";
    $content .= "  Line: $errline\n";
    if (STACK_TRACE) $content .= "Trace: $trace\n";

    return $content;
}

register_shutdown_function( "fatal_handler" );
set_error_handler( "non_fatal_handler" );
