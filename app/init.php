<?php
// Get $CONF array()
if (file_exists(BASE . '/app/config/config.php')) include_once( BASE . '/app/config/config.php' );
else die('Configuration File Missing. Exiting.');

// Init Error Handler
include( BASE . '/app/error_handler.php');

// Init Engine
include_once( BASE . '/app/base/core/engine.php');
?>
