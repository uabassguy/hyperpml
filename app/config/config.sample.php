<?php
//Init $CONF
global $CONF;
$CONF = array();

//Core Database Definitions
$CONF['DB']['USER'] = "";
$CONF['DB']['PASS'] = "";
$CONF['DB'][ 'DB' ] = "";
$CONF['DB']['HOST'] = "localhost";

/* DEV_MODE
 * enables use of developer features (see below)
 */
define('DEV_MODE', true);

/* LOG_MODE enables logging errors to files in the path: log/
 * Requires DEV_MODE to be true
 */
define('LOG_MODE', true);

/* VERBOSE_MODE
 * true shows errors in the html markup, false hides them. Requires DEV_MODE to be true
 */
define('VERBOSE_MODE', true);

/* STACK_TRACE
 * Enable stack trace, for added verbosity. Requires DEV_MODE to be true
 * WARNING: Can cause very large output, as it will show all collected variables in the stack!
 * Outputs all variables to error file, and screen if VERBOSE_MODE is true. Use only as needed
 */
define('STACK_TRACE', false);

//CSS Options. Gzip does nothing yet
define('CSS_MIN', true);
define('CSS_GZIP', false);
define('CSS_SERV_CACHE', false); //recommended for production, set false for development
define('CSS_REFRESH_RATE', 5); //refresh server css cache (in minutes)

?>
