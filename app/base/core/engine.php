<?php
/* HPML Core Engine
 * Created by Dave Wilson
 * This file loads the core, and includes other files needed for the core using values from XML
 * This file also collects a list of other xml files in app/base/core/xml and loads those as well
 */

//auto loader
function __autoload($class){
    $file = str_replace('_', '/class/', $class) . '.php';
    $corefile = BASE.'/app/base/core/'.$file;
    $packagefile = BASE.'/app/base/package/'.$file;
    $customfile = BASE.'/app/base/custom/'.$file;
    if (file_exists($customfile)) {
        include_once($customfile);
    }
    elseif (file_exists($packagefile)) {
        include_once($packagefile);
    }
    elseif (file_exists($corefile)) {
        include_once($corefile);
    }
    else {
        die($corefile.' Not Found.');
    }
}

// Init core
$core = new hpml_core();

// Init session
$core->session->start_session('_s', false);

// Write output
$core->finish();
?>
