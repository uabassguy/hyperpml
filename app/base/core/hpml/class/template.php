<?php
class hpml_template extends hpml_controller {

    public function __construct(){
        $this->getRouter();
        $this->getRequest();
    }

    public function getTemplate(){
        $this->defaultTemplate = BASE."/app/view/html/index.hpml";
        $this->basePath = '/'.$this->router->base.'/';
        $this->theRequest = str_replace($this->basePath, '', $this->request);
        $theRequest = $this->theRequest;
        if ($this->router->$theRequest) {
            $this->template = $this->router->$theRequest->template;
        }
	else if (empty($theRequest)) {
	    $this->template = 'index';
	}
        else {
            $this->template = $this->router->null->template;
        }
        $this->templateFile = BASE.'/app/view/html/'.$this->template.'.hpml';

        if (file_exists($this->templateFile)){
            $this->output = file_get_contents($this->templateFile);
        }
        else {
            $this->output = file_get_contents($this->defaultTemplate);
        }
        return $this->template;
    }

    public function parse(){
        global $core;
        $this->getTemplate();
        $this->operator = '!';
        $this->escape = "(?<![".$this->operator."])";
        //get any pre-defined requirements for template
        //sanitize template
        //init classes
        if(preg_match_all("/".$this->escape."{\+(.*).}/", $this->output, $matches)){
            foreach($matches[0] as $purge){
                $this->output = str_replace($purge,'',$this->output);
            }
            foreach($matches[1] as $class){
                $core->$class = new $class;
            }
        }

        $template_regex = "/".$this->escape."{template (.*)}/";
        if(preg_match_all($template_regex, $this->output, $matches)){//print_r($matches);die();
            $i = 0;
            foreach($matches[0] as $swap){
                $file = BASE.'/app/view/html/'.$matches[1][$i].'.hpml';
                if (file_exists($file)){
                    $new = file_get_contents($file);
                    $this->output = str_replace($swap,$new,$this->output);
                }
                $i++;
            }
        }
        $url_regex = "/".$this->escape."{url (.*)}/";
        if(preg_match_all($url_regex, $this->output, $matches)){
            $i = 0;
            foreach($matches[0] as $swap){
                $new = BASE_URL.$this->basePath.$matches[1][$i];
                $this->output = str_replace($swap,$new,$this->output);
                $i++;
            }
        }
        //get variables
        //replace with data
        $data_model_regex = "/".$this->escape."{do (.*)}/";
        //$data_model_delimiter_open = "/".$this->escape."({do)/";
        if(preg_match_all($data_model_regex, $this->output, $matches)){
            foreach($matches[0] as $swap){
                $old = $swap;
                $swap = str_replace('/','->',$swap);
                $swap = str_replace('{do ','<?php echo $this->',$swap);
                $swap = str_replace('}','(); ?>',$swap);
                $this->output = str_replace($old, $swap,$this->output);
            }
        }
        //clean escape delimiters from the output, if any.
        $this->output = str_replace($this->operator.'{','{',$this->output);
        $this->output = str_replace($this->operator.'}','}',$this->output);
        return $this->output;
    }
}
