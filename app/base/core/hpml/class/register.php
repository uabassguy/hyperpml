<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dave
 * Date: 10/23/13
 * Time: 11:51 PM
 * To change this template use File | Settings | File Templates.
 */

class hpml_register {
    public $xml,$xml_arr;
    function __construct(){
        $this->xml_arr = array();

        foreach (glob(BASE . '/app/config/xml/*.xml') as $file) {
            $this->filename = basename($file);
            $this->XML($file);
            $this->xml_arr[$this->filename] = array( 'data'=>$this->xml );
        }
        $this->autoload = $this->xml_arr['config.xml']['data']->autoload->load;
    }
    function XML($file){
	    $this->xml = simplexml_load_file($file);
        return $this->xml;
    }
}