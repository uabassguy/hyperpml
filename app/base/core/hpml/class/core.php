<?php
/* Html construction classes for views
 *
 */

class hpml_core {
    public $core, $db, $css, $controller, $register, $helper;
    function __construct() {
        $this->hpml = new hpml_general();
        $this->register = new hpml_register();
        $this->db = new hpml_db();
        $this->css = new hpml_css();
        $this->controller = new hpml_controller();
        $this->session = new hpml_session();
        $this->template = new hpml_template();
    }
    public function getCoreFile($name) {
        return $this->hpml->getCoreFile($name);
    }
    public static function helper($class) {
        $helper = new $class();
        return $helper;
    }
    public function get($class) {
        $this->$class = new $class();
    }
    public function finish() {
        $out = "?>".$this->template->parse()."<?php ";
        $out = str_replace('core->','this->',$out);
        eval($out);
    }
}
