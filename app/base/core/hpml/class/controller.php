<?php

class hpml_controller {
    public $request, $router;
    public function __construct() {

    }
    public function getRequest() {
        $this->request = $_SERVER['REQUEST_URI'];
        //$this->request = $_GET;
        return $this->request;
    }
    public static function getStaticRouter(){
        $router = hpml_core::helper('hpml_register')->xml_arr['router.xml']['data'];
        return $router;
    }
    public function getRouter(){
        $this->router = hpml_core::helper('hpml_register')->xml_arr['router.xml']['data'];
        return $this->router;
    }
}

