<?php
/**
 * On-the-fly CSS Compression
 * Copyright (c) 2009 and onwards, Manas Tungare.
 * Creative Commons Attribution, Share-Alike.
 *
 * In order to minimize the number and size of HTTP requests for CSS content,
 * this script combines multiple CSS files into a single file and compresses
 * it on-the-fly.
 *
 * To use this in your HTML, link to it in the usual way:
 * <link rel="stylesheet" type="text/css" media="screen, print, projection" href="/css/compressed.css.php" />
 */

class hpml_css extends hpml_general{
    public $gzip, $minify, $filename, $servedfile;
    public function __construct(){
        parent::__construct();
        $this->gzip = CSS_GZIP;
        $this->minify = CSS_MIN;
        $this->filename = BASE.'/cached.css';
        $this->servedfile = BASE_URL.'/cached.css';
    }

    public function collectCss(){
        if ( CSS_SERV_CACHE && ( time() - filemtime($this->filename) < CSS_REFRESH_RATE * 60 ) ) return false;
        $cssFiles = array();
        $cssFiles = glob(BASE . '/app/view/css/*.css');
        return $cssFiles;
    }
    public function processCss(){
        if ( CSS_SERV_CACHE && ( time() - filemtime($this->filename) < CSS_REFRESH_RATE * 60 ) ) return false;
        $this->buffer = "";
        foreach ($this->cssFiles as $cssFile) {
            $this->buffer .= file_get_contents($cssFile);
        }
        if($this->minify) $this->minifyCss();
        return true;
    }
    public function minifyCss(){
        if ( CSS_SERV_CACHE && ( time() - filemtime($this->filename) < CSS_REFRESH_RATE * 60 ) ) return false;

        // Remove comments
        $this->buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $this->buffer);

        // Remove space after colons
        $this->buffer = str_replace(': ', ':', $this->buffer);

        // Remove whitespace
        $this->buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $this->buffer);

        if ($this->gzip) {
            // Enable GZip encoding.
            ob_start("ob_gzhandler");
        }

        // Enable caching
        //header('Cache-Control: public');

        // Expire in one day
        //header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 86400) . ' GMT');

        // Set the correct MIME type, because Apache won't set it for us
        //header("Content-type: text/css");

        // Write everything out
        return $this->buffer;
    }
    public function getCss(){
        if (empty($this->buffer)) {
            $this->cssFiles = $this->collectCss();
            if ( !$this->processCss() ) return false;
        }
        return $this->buffer;
    }
    public function go(){
        if (empty($this->cssFiles)) {
            $this->getCss();
        }
        //if server css cache is on, check how old the file is, and update as needed, then serve
        if ( CSS_SERV_CACHE ){
            $out = "<LINK href='".$this->servedfile."' rel='stylesheet' type='text/css'>";
            if ( time() - filemtime($this->filename) > CSS_REFRESH_RATE * 60 ){
                file_put_contents(BASE.'/cached.css',$this->buffer);
            }
            return $out;
        }
        //else just output consolidated css buffer
        else {
            $out = "<style type='text/css'>".$this->buffer."</style>";
        }
        return $out;
    }
}