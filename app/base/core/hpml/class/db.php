<?php
/* Database Interface
 *
 */

class hpml_db extends hpml_general {
    public $db;
    public function __construct() {
        parent::__construct();
        $this->getDb();
    }
    public function getDb() {
        $args = $this->CONF['DB'];
        $this->db = new mysqli($args['HOST'],$args['USER'],$args['PASS'],$args['DB']);
        if (mysqli_connect_errno()) {
            die('Failed to connect to database.');
        }
        else return true;
    }
    /* function query($string)
     * @returns mysqli_result Object
     */
    function query($string) {
        $q = $this->db->query($string);
        $result = $q->fetch_assoc();
        return $result;
    }
    function close() {
        $this->db->close();
    }
}