<?php
/*  class Hpml_Core
 *  Created by Dave Wilson
  */

class hpml_general {
    /* function getCoreFile($file)
     * @accepts string
     * @returns string
     */
    public $customfile,$packagefile,$corefile,$file,$db,$CONF;
    public function __construct(){
        $this->CONF = $GLOBALS['CONF'];
    }
    public function getCoreFile($file){
        //if(empty($file)) { return 'null'; }
        $this->customfile = BASE . '/app/base/custom/' . $file . '.php';
        $this->packagefile = BASE . '/app/base/package/' . $file . '.php';
        $this->corefile = BASE . '/app/base/core/' . $file . '.php';
        if ( file_exists($this->customfile) ) {
            return $this->customfile;
        }
        else if ( file_exists($this->packagefile) ) {
            return $this->packagefile;
        }
        else if ( file_exists($this->corefile) ) {
            return $this->corefile;
        }
	    else die("Invalid call, file $this->corefile does not exist in repo.");return;
    }
    public function getPackage($name){
        $this->packagefile = BASE . '/app/base/package/' . $file . '.php';
        if ( file_exists($this->packagefile) ) {
            return $this->packagefile;
        }
        else die("Invalid call, file $this->packagefile does not exist in repo.");return;
    }
    /* getConfig()
     * returns array( $CONF )
     */
    public function getConfig() {
        return $this->CONF;
    }

    public function get($args) {
        if(isset($args) && !empty($args)){
            if (strstr($args, ',')){
                $this->params = preg_split("/[,]+/", $args);
            }
            else {
                $this->params = array("$args");
            }
            return true;
        }
        else {
            return false;
        }
    }
}
