#Hyper PHP Markup Language

An easy to use PHP framework, with a central focus on only loading data for a specific view for less overhead

##Current features:
 - Class Autoloader
 - Small footprint
 - XML Parsing
 - CSS Consolidation/Minifying built in
 - Uses mysqli

##App Folder structure:
    base		- Code location
     /core 		- Core code
     /package   - Packages overrides Core
     /custom	- Overrides Packages and Core
 
##Goals:
Use an alternate structure to parse template files, using only the classes and components necessary to output the data, creating a small footprint.

##Sample Syntax:
    {+path_requiredclass.}
    <!DOCTYPE HTML>
    <head>
        {do core/showCss}
    </head>
    <body>
    {template menu}
    Welcome to HyperPml!
    </body>
    
(Licensed under Creative Commons Attribution License)[http://creativecommons.org/licenses/by/3.0/us/]