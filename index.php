<?php
/* Welcome to HyperPml
 * By Dave Wilson 2013
 * Creative Commons Attribution License
 */
// Set base folder for includes
define( 'BASE', getcwd() );
define( 'BASE_URL', 'http://'.$_SERVER['SERVER_NAME']);
// Init app
include( BASE . '/app/init.php' );
?>
